from django.contrib import admin
from django.urls import path
from .views import Test, Producto, Orden, Usuario, Reporte
urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', Test.as_view()),
    path('productos/', Producto.as_view()),
    path('ordenes/', Orden.as_view()),
    path('usuarios/', Usuario.as_view()),
    path('reporte/', Reporte.as_view()),
]
