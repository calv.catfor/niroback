from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Product,ProductCategory,Orders

class ProductCategorySerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        ModelClass=ProductCategory  # or whichever desired model you are acting on
        try:
            instance = ModelClass.objects.create(**validated_data)
        except TypeError: # or whatever error type you are mitigating against, if any
            raise TypeError()
        return instance
    class Meta:
        model= ProductCategory
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        ModelClass=Product  # or whichever desired model you are acting on
        try:
            instance = ModelClass.objects.create(**validated_data)
        except TypeError: # or whatever error type you are mitigating against, if any
            raise TypeError()
        return instance
    class Meta:
        model= Product
        fields = '__all__'

class OrdersSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        ModelClass=Orders  # or whichever desired model you are acting on
        try:
            instance = ModelClass.objects.create(**validated_data)
        except TypeError: # or whatever error type you are mitigating against, if any
            raise TypeError()
        return instance
    class Meta:
        model= Orders
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        ModelClass=User  # or whichever desired model you are acting on
        try:
            instance = ModelClass.objects.create(**validated_data)
        except TypeError: # or whatever error type you are mitigating against, if any
            raise TypeError()
        return instance
    class Meta:
        model= User
        fields = '__all__'