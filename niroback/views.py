import logging, os, sys, random, datetime
from django.db.models import Sum
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.db.models.functions import ExtractDay, ExtractHour, ExtractMinute, ExtractMonth, ExtractQuarter, ExtractSecond, ExtractWeek, ExtractIsoWeekDay, ExtractWeekDay, ExtractIsoYear, ExtractYear
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.views import APIView
from .models import ProductCategory,Product,Orders
from .serializers import ProductSerializer, UserSerializer, ProductCategorySerializer, OrdersSerializer

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] > %(message)s')
logger = logging.getLogger(__name__)

def LoggerError(pseudoid = None):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    err = f"{('[{}] - '.format(pseudoid) if pseudoid else '')}LoggerERROR {exc_type}, {exc_tb.tb_frame.f_code.co_filename}, {exc_tb.tb_lineno}"
    logger.error(err)
    return err

def get_id():
    return random.randint(0,9000)

class Test(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def delete(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class ProductoCategoria(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_201_CREATED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def delete(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Producto(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            products = Product.objects.filter(deleted=False).order_by("category","name").values()
            productscategory = ProductCategory.objects.filter(deleted=False).order_by("name").values()
            data = dict()
            data["products"] = products
            data["productscategory"] = productscategory
            return Response(data, status=status.HTTP_200_OK)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        logger.info(f"DATOS DEL POST DE PRODUCTOS {request.data}")
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            name = request.data.get('name')
            category = request.data.get('category')
            count = request.data.get('count')
            price = request.data.get('price')
            datos = {'name' : name, 'category' : category, 'count' : count, 'price' : price}
            product = ProductSerializer(data=datos)
            if product.is_valid():
                product.save()
                return Response(None, status=status.HTTP_201_CREATED)
            logger.info(product.errors)
            return Response(product.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            logger.info(request.data)
            producto = Product.objects.get(id=request.data.get('idp'))
                
            name = request.data.get('name')
            category = request.data.get('category')
            count = request.data.get('count')
            price = request.data.get('price')
            if request.data.get('delete'):
                datos = {id : producto.id, 'name' : producto.name, 'category' : producto.category.id, 'count' : producto.count, 'price' : producto.price,'deleted' : True}
            else:
                datos = {id : producto.id, 'name' : name, 'category' : category, 'count' : count, 'price' : price}
            product = ProductSerializer(producto,data=datos)
            if product.is_valid():
                product.save()
                return Response(None, status=status.HTTP_201_CREATED)
            logger.info(product.errors)
            return Response(product.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Usuario(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            # if request.headers.get("isStaff"):
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            usuarios = User.objects.filter(is_active=True).values()
            data = dict()
            data["usuarios"] = usuarios
            return Response(data, status=status.HTTP_200_OK)
            # return Response(None, status=status.HTTP_403_FORBIDDEN)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            # if request.headers.get("isStaff"):
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            name = request.data.get("username")
            email = request.data.get("email")
            password = request.data.get("password")
            datos = {'username': name, 'email': email, 'password': password}
            user = UserSerializer(data=datos)
            if user.is_valid():
                user.save()
                return Response(request.data, status=status.HTTP_201_CREATED)
            else:
                logger.info(user.errors)
                return Response(user.errors, status=status.HTTP_400_BAD_REQUEST)
            # return Response(None, status=status.HTTP_403_FORBIDDEN)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            if request.headers.get("isStaff"):
                logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
                usuario = User.objects.get(id=request.data.get('id'))
                name = request.data.get("name")
                email = request.data.get("email")
                password = request.data.get("password")
                datos = {'username': name, 'email': email, 'password': password}
                user = UserSerializer(usuario,data=datos)
                if user.save():
                    return Response(request.data, status=status.HTTP_201_CREATED)
                else:
                    logger.info(user.errors)
                    return Response(user.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(None, status=status.HTTP_403_FORBIDDEN)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Orden(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            if request.headers.get("pkuser"):
                orders = Orders.objects.filter(user=request.headers.get("pkuser"), deleted=False).order_by("datetime").annotate(year=ExtractYear('datetime'),month=ExtractMonth('datetime'),day=ExtractDay('datetime'),hour=ExtractHour('datetime'),minute=ExtractMinute('datetime'),second=ExtractSecond('datetime')).values("id","user__id","user__username","client","service","products__id","products__name","count","total","datetime","year","month","day","hour","minute","second")
                if len(orders) < 1:
                    return Response(None, status=status.HTTP_204_NO_CONTENT)
                logger.debug(orders)
                data = dict()
                products = Product.objects.filter(deleted=False).order_by("category","name").values()
                data["products"] = products
                data["ordenes"] = orders
                return Response(data, status=status.HTTP_200_OK)
            return Response(None, status=status.HTTP_428_PRECONDITION_REQUIRED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            usrid=request.headers.get("pkuser")
            prod = Product.objects.get(id=request.data.get("product"))
            client = request.data.get("client")
            service = request.data.get("service")
            amount = request.data.get("amount")
            total = prod.price * int(amount)
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            datos = { 'user':usrid,'service':service, 'client':client,'products':prod.id,'count':amount,'total':total, 'deleted':(True if request.data.get("delete") else False) }
            orden = OrdersSerializer(data=datos)
            if orden.is_valid():
                orden.save()
                return Response(None, status=status.HTTP_201_CREATED)
            logger.info(orden.errors)
            return Response(orden.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_202_ACCEPTED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def delete(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_202_ACCEPTED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Reporte(APIView):

    def get(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            if request.headers.get("pkuser"):
                
                orders = Orders.objects.values("products__name").annotate(Sum('total'),Sum('count')).order_by('-count__sum')
                if len(orders) < 1:
                    return Response(None, status=status.HTTP_204_NO_CONTENT)
                logger.debug(orders)
                data = dict()
                data["ordenes"] = orders
                return Response(data, status=status.HTTP_200_OK)
            return Response(None, status=status.HTTP_428_PRECONDITION_REQUIRED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            if request.headers.get("pkuser"):
                
                start_date = request.data.get("ini")
                print(f"start_date - {type(start_date)}")
                end_date = request.data.get("end")
                print(f"end_date - {type(end_date)}")
                orders = Orders.objects.filter(datetime__gte=start_date,datetime__lte=end_date).values("products__name").annotate(Sum('total'),Sum('count')).order_by('-count__sum')
                if len(orders) < 1:
                    return Response(None, status=status.HTTP_204_NO_CONTENT)
                logger.debug(orders)
                data = dict()
                data["ordenes"] = orders
                return Response(data, status=status.HTTP_200_OK)
            return Response(None, status=status.HTTP_428_PRECONDITION_REQUIRED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def put(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_202_ACCEPTED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
    def delete(self, request, *args, **kwargs):
        pseudoid = get_id()
        try:
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return Response(request.data, status=status.HTTP_202_ACCEPTED)
        except:
            return Response(LoggerError(pseudoid), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
