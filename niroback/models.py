from django.db import models
from django.contrib.auth.models import User

class ProductCategory(models.Model):
    '''
        Modelo base para categorizar los productos
    '''
    name = models.CharField(max_length=50, blank=False, null=False)
    active = models.BooleanField(default=True, null=False)
    deleted = models.BooleanField(default=False)

class Product(models.Model):
    '''
        Modelo base para la abstraccion de los productos
    '''
    name = models.CharField(max_length=50, blank=False, null=False)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    count = models.IntegerField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
    deleted = models.BooleanField(default=False)

class Orders(models.Model):
    '''
        Modelo base para el control de las ventas
    '''
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    client = models.CharField(max_length=80, blank=False, null=False)
    service = models.CharField(max_length=40, blank=False, null=False)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    count = models.IntegerField()
    total = models.DecimalField(max_digits=6, decimal_places=2)
    datetime = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)