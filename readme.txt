> 1 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Abrir la consola a la altura del archivo manage.py, copiar las 4 líneas de comando y pegar en consola:
> 1 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

py manage.py makemigrations niroback
py manage.py migrate
py manage.py shell

> 2 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#En caso de no cargar el archivo python para inicializar la BD se puede abrir la shell de django con la siguiente linea
    py manage.py shell
Se abrira una consola de "Django"/"Python", y ya una vez dentro de la Shell de Django, ejecutar las siguientes lineas:
> 2 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

from niroback.models import ProductCategory ,Product ,Orders
from django.contrib.auth.models import User
import datetime

User.objects.create_user(username='admin', email='admin@parrotsoftware.io', password='1234567890')
User.objects.create_user(username='christian', email='christian.lopez@parrotsoftware.io', password='1234567890')

pc1 = ProductCategory(name='Entrada')
pc1.save()
pc2 = ProductCategory(name='Plato Fuerte')
pc2.save()
pc3 = ProductCategory(name='Postres')
pc3.save()
pc4 = ProductCategory(name='Bebidas')
pc4.save()

p1 = Product(name='Arroz Con Platano', category = pc1, count = 10, price = 40)
p1.save()
p2 = Product(name='Caldo De Pollo', category = pc1, count = 5, price = 45)
p2.save()
p3 = Product(name='Arrachera Con Papas', category = pc2, count = 15, price = 105)
p3.save()
p6 = Product(name='Pechuga Empanizada', category = pc2, count = 60, price = 85)
p6.save()
p7 = Product(name='Filete De Pescado Al Mojo de Ajo', category = pc2, count = 20, price = 95)
p7.save()
p4 = Product(name='Flan Napolitano', category = pc3, count = 100, price = 23)
p4.save()
p5 = Product(name='Agua de Sabor', category = pc4, count = 100, price = 18)
p5.save()
p6 = Product(name='Cerveza', category = pc4, count = 100, price = 22)
p6.save()

usr = User.objects.get(id=1)
prod = Product.objects.get(id=1)
ord1 = Orders(user=usr, service="M1",client="Jose Luis",products=p3,count=2,total=( 2 * p3.price))
ord1.save()
ord1.datetime=datetime.datetime(2021, 4, 19, 11, 20)
ord1.save()
ord2 = Orders(user=usr, service="T2",client="Christian Valdez",products=p1,count=1,total=( 1 * p1.price))
ord2.save()
ord2.datetime=datetime.datetime(2021, 4, 19, 11, 20)
ord2.save()
ord3 = Orders(user=usr, service="M1",client="Jose Luis - Acompañante",products=p2,count=4,total=( 4 * p2.price))
ord3.save()
ord3.datetime=datetime.datetime(2021, 4, 19, 11, 20)
ord3.save()
ord4 = Orders(user=usr, service="M1",client="Jose Luis",products=p7,count=2,total=( 2 * p7.price))
ord4.save()
ord4.datetime=datetime.datetime(2021, 4, 19, 11, 20)
ord4.save()
ord5 = Orders(user=usr, service="T2",client="Christian Valdez",products=p3,count=4,total=( 4 * p3.price))
ord5.save()
ord5.datetime=datetime.datetime(2021, 4, 19, 11, 20)
ord5.save()
ord6 = Orders(user=usr, service="S1",client="Cindy Lopez",products=p5,count=2,total=( 2 * p5.price))
ord6.save()
ord6.datetime=datetime.datetime(2021, 4, 23, 11, 20)
ord6.save()
ord7 = Orders(user=usr, service="S2",client="Eliza Agustin",products=p1,count=1,total=( 1 * p1.price))
ord7.save()
ord7.datetime=datetime.datetime(2021, 4, 23, 11, 20)
ord7.save()
ord8 = Orders(user=usr, service="K4",client="Eduardo Martinez - Acompañante",products=p2,count=4,total=( 4 * p2.price))
ord8.save()
ord8.datetime=datetime.datetime(2021, 4, 23, 11, 20)
ord8.save()
ord9 = Orders(user=usr, service="S4",client="Salvador Martinez",products=p1,count=2,total=( 2 * p1.price))
ord9.save()
ord9.datetime=datetime.datetime(2021, 4, 23, 11, 20)
ord9.save()
ord10 = Orders(user=usr, service="S1",client="Cindy Lopez",products=p6,count=4,total=( 4 * p6.price))
ord10.save()
ord10.datetime=datetime.datetime(2021, 4, 23, 11, 20)
ord10.save()
exit()

> 3 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Para correr el proyecto:
> 3 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

py manage.py runserver

